export interface PaginationOptionsInterface {
  mahasiswa?: string;
  dosen?: string;
  course?: string;
  search?: string;
  name?: string;
  limit: number;
  page: number;
}
