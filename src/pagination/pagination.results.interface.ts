export interface PaginationResultInterface<PaginationEntity> {
  message?: string;
  total: number;
  data: PaginationEntity[];
  status?: number;
  last_page?: number;
  next?: string;
  previous?: string;
}
