import { PaginationResultInterface } from './pagination.results.interface';

export class Pagination<PaginationEntity> {
  public total: number;
  public message?: string;
  public data: PaginationEntity[];
  public page_total: number;
  public status?: number;
  public last_page?: number;

  constructor(paginationResults: PaginationResultInterface<PaginationEntity>) {
    this.status = paginationResults.status;
    this.message = paginationResults.message;
    this.total = paginationResults.total;
    this.last_page = paginationResults.last_page;
    this.data = paginationResults.data;
    // this.page_total = paginationResults.data.length;
  }
}
