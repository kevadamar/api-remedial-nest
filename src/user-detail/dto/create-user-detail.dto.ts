import { IsNumber, IsString } from 'class-validator';

export class CreateUserDetailDto {
  @IsNumber()
  id?: number;

  @IsString()
  gelar?: string;

  @IsNumber()
  semester_aktif?: number;

  @IsString()
  gender?: string;

  @IsString()
  japung?: string; // jabatan fungsional

  @IsString()
  hp?: string;

  @IsString()
  address?: string;

  @IsString()
  tempat_lahir?: string;

  @IsString()
  tanggal_lahir?: Date;

  @IsString()
  agama?: string;

  @IsString()
  nik?: string;

  @IsString()
  nama_ibu?: string;

  @IsString()
  nik_ibu?: string;

  @IsString()
  nama_ayah?: string;

  @IsString()
  nik_ayah?: string;

  @IsString()
  tahun_akademik?: string;

  @IsNumber()
  prodiId?: number;
}
