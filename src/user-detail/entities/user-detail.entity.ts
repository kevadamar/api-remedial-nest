import { Prodi } from 'src/prodi/entities/prodi.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('user_detail')
export class UserDetail {
  @PrimaryGeneratedColumn('increment')
  id: number;

  // @Column()
  // name: string;

  @Column({ nullable: true })
  gelar: string;

  @Column({ nullable: true })
  semester_aktif: number;

  @Column({ nullable: true })
  gender: string;

  @Column({ nullable: true })
  japung: string; // jabatan fungsional

  @Column({ nullable: true })
  hp: string;

  @Column({ nullable: true, type: 'text' })
  address: string;

  @Column({ nullable: true })
  tempat_lahir: string;

  @Column({ nullable: true, type: 'date' })
  tanggal_lahir: Date;

  @Column({ nullable: true })
  agama: string;

  @Column({ nullable: true })
  nik: string;

  @Column({ nullable: true })
  nama_ibu: string;

  @Column({ nullable: true })
  nik_ibu: string;

  @Column({ nullable: true })
  nama_ayah: string;

  @Column({ nullable: true })
  nik_ayah: string;

  @Column({ nullable: true })
  tahun_akademik: string;

  @Column({ default: false, select: false })
  is_deleted: boolean;

  @Column({ select: false, type: 'timestamp' })
  updated_at: Date;

  @Column({
    select: false,
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
  })
  created_at: Date;

  //   fk

  @Column({ default: null, select: false })
  prodiId: number;

  //   relations
  @ManyToOne(() => Prodi, (prodi) => prodi.userDetail)
  @JoinColumn()
  prodi: Prodi;

  @OneToOne(() => User, (user) => user.userDetail)
  user: User;
}
