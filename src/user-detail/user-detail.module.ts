import { Module } from '@nestjs/common';
import { UserDetailService } from './user-detail.service';
import { UserDetailController } from './user-detail.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserDetailRepository } from './user-detail.repository';

@Module({
  controllers: [UserDetailController],
  providers: [UserDetailService],
  imports: [TypeOrmModule.forFeature([UserDetailRepository])]
})
export class UserDetailModule {}
