import {
  Controller,
  Post,
  Body,
  UseGuards,
  Req,
  Get,
  HttpStatus,
} from '@nestjs/common';
import { PermissionsGuard } from 'src/permissions.guard';
import { AuthService } from './auth.service';
import { CreateAuthDto } from './dto/auth-credentials.dto';
import { JwtAuthGuard } from './jwt.auth.guard';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  async login(@Body() authCredentials: CreateAuthDto): Promise<object> {
    return await this.authService.login(authCredentials);
  }

  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @Get('profile')
  async profile(@Req() req): Promise<object> {
    const data = await this.authService.profile(req.user);

    return {
      statusCode: HttpStatus.OK,
      message: 'Success Get Profile',
      data,
    };
  }
}
