import {
  HttpException,
  HttpStatus,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { User } from 'src/users/entities/user.entity';
import { UsersService } from 'src/users/users.service';
import { CreateAuthDto } from './dto/auth-credentials.dto';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async profile(user: User): Promise<User> {
    return await this.usersService.findOne(user.id);
  }

  async login(authLoginDto: CreateAuthDto) {
    const user = await this.validateUser(authLoginDto);

    if (user.is_deleted) {
      throw new HttpException('User Not Active', HttpStatus.BAD_REQUEST);
    }

    const payload = {
      id: user.id,
      full_name: user.full_name,
      email: user.email,
      username: user.username,
      nim: user.nim,
      role: user.role.name,
      roleId: user.role.id,
      userDetail: user?.userDetail,
    };

    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  async validateUser(authLoginDto: CreateAuthDto): Promise<User> {
    const { email, password } = authLoginDto;

    const user = await this.usersService.login(email);

    if (!(await user?.validatePassword(password, user.password))) {
      throw new UnauthorizedException();
    }

    return user;
  }
}
