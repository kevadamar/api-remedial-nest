import { IsNotEmpty } from 'class-validator';

export class CreateCourseDto {
  @IsNotEmpty()
  code: string;

  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  kurikulum_semester: string;

  @IsNotEmpty()
  semester: number;

  @IsNotEmpty()
  sks: number;

  @IsNotEmpty()
  dosenId: number;
}
