import { Nilai } from 'src/nilai/entities/nilai.entity';
import { Remedial } from 'src/remedial/entities/remedial.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('course')
export class Course {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  code: string;
  @Column() // kode matkul
  name: string;
  @Column()
  semester: number;
  @Column()
  sks: number;
  @Column()
  kurikulum_semester: string;
  @Column({ default: false })
  is_deleted: boolean;

  // fk
  @Column({ nullable: true })
  dosenId: number;

  @Column({ type: 'timestamp' })
  updated_at: Date;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  created_at: Date;

  // relations
  @OneToMany(() => Nilai, (nilai) => nilai.course)
  @JoinColumn()
  nilai: Nilai[];

  @OneToMany(() => Remedial, (remedial) => remedial.course)
  @JoinColumn()
  remedial: Remedial[];

  @ManyToOne(() => User, (user) => user.course)
  @JoinColumn()
  dosen: User;
}
