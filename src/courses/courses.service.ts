import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CourseRepository } from './courses.repository';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';
import { Course } from './entities/course.entity';

@Injectable()
export class CoursesService {
  constructor(
    @InjectRepository(CourseRepository)
    private readonly courseRepository: CourseRepository,
  ) {}

  async create(createCourseDto: CreateCourseDto): Promise<Course> {
    // create courses and return created courses
    const course = await this.courseRepository.save(createCourseDto);
    return course;
  }

  async findAll(semester: number): Promise<Course[]> {
    // let param = {
    //   is_deleted: false,
    // };

    // if (semester) {
    //   param = { ...param, semester };
    // }

    const whereCon = !semester
      ? {
          is_deleted: false,
        }
      : { is_deleted: false, semester };
    const course = await this.courseRepository.find({
      where: whereCon,
      relations: ['dosen'],
      order: {
        id: 'DESC',
      },
    });

    return course;
  }

  async findOne(id: number): Promise<Course> {
    // find courses by id
    const course = await this.courseRepository.findOne(id, {
      relations: ['dosen'],
    });
    return course;
  }

  async update(id: number, updateCourseDto: UpdateCourseDto): Promise<object> {
    // Update Courses by id
    // and return updated courses
    const course = await this.courseRepository.update(id, updateCourseDto);
    return course;
  }

  remove(id: number) {
    return `This action removes a #${id} course`;
  }
}
