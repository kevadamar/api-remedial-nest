import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { PaymentProofService } from './payment-proof.service';
import { CreatePaymentProofDto } from './dto/create-payment-proof.dto';
import { UpdatePaymentProofDto } from './dto/update-payment-proof.dto';

@Controller('payment-proof')
export class PaymentProofController {
  constructor(private readonly paymentProofService: PaymentProofService) {}

  @Post()
  create(@Body() createPaymentProofDto: CreatePaymentProofDto) {
    return this.paymentProofService.create(createPaymentProofDto);
  }

  @Get()
  findAll() {
    return this.paymentProofService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.paymentProofService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updatePaymentProofDto: UpdatePaymentProofDto) {
    return this.paymentProofService.update(+id, updatePaymentProofDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.paymentProofService.remove(+id);
  }
}
