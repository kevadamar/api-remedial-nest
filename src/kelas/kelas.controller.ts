import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpCode,
  Res,
  HttpStatus,
  Req,
} from '@nestjs/common';
import { KelasService } from './kelas.service';
import { CreateKelaDto } from './dto/create-kela.dto';
import { UpdateKelaDto } from './dto/update-kela.dto';
import { Kelas } from './entities/kelas.entity';
import { Pagination } from 'src/pagination/pagination';

@Controller('kelas')
export class KelasController {
  constructor(private readonly kelasService: KelasService) {}

  @Post()
  @HttpCode(201)
  async create(@Body() createKelaDto: CreateKelaDto): Promise<object> {
    const data = await this.kelasService.create(createKelaDto);
    return {
      statusCode: HttpStatus.CREATED,
      message: 'Success Created',
      data,
    };
  }

  @Get()
  async findAll(@Req() request): Promise<Pagination<Kelas>> {
    return await this.kelasService.findAll({
      name: request.query.hasOwnProperty('name') ? request.query.name : '',
      limit: request.query.hasOwnProperty('size')
        ? parseInt(request.query.size)
        : 10,
      page: request.query.hasOwnProperty('page')
        ? parseInt(request.query.page) - 1
        : 0,
    });
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<object> {
    const data = await this.kelasService.findOne(+id);
    return {
      statusCode: HttpStatus.OK,
      message: 'Success Get',
      data,
    };
  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateKelaDto: UpdateKelaDto,
  ): Promise<object> {
    const data = await this.kelasService.update(+id, updateKelaDto);
    return {
      statusCode: HttpStatus.OK,
      message: 'Success Update',
      data,
    };
  }

  @Delete(':id')
  async remove(@Param('id') id: string): Promise<object> {
    const data = await this.kelasService.remove(+id);
    return {
      statusCode: HttpStatus.OK,
      message: 'Success Delete',
      data,
    };
  }
}
