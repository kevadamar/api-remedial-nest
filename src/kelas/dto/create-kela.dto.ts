import { IsString } from 'class-validator';

export class CreateKelaDto {
  @IsString()
  name: string;
}
