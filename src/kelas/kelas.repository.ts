import { EntityRepository, Repository } from 'typeorm';
import { Kelas } from './entities/kelas.entity';

@EntityRepository(Kelas)
export class KelasRepository extends Repository<Kelas> {}
