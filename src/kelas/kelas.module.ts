import { Module } from '@nestjs/common';
import { KelasService } from './kelas.service';
import { KelasController } from './kelas.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KelasRepository } from './kelas.repository';

@Module({
  controllers: [KelasController],
  providers: [KelasService],
  imports: [TypeOrmModule.forFeature([KelasRepository])],
})
export class KelasModule {}
