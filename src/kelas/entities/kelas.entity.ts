import { User } from 'src/users/entities/user.entity';
import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('kelas')
export class Kelas {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  name: string;

  @Column({ select: false, default: false })
  is_deleted: boolean;

  @Column({ type: 'timestamp' })
  updated_at: Date;

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
  })
  created_at: Date;

  //   relations
  @OneToMany(() => User, (user) => user.kelas)
  @JoinColumn()
  user: User[];
}
