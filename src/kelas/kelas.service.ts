import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Pagination } from 'src/pagination/pagination';
import { PaginationOptionsInterface } from 'src/pagination/pagination.options.interface';
import { getConnection, Like } from 'typeorm';
import { CreateKelaDto } from './dto/create-kela.dto';
import { UpdateKelaDto } from './dto/update-kela.dto';
import { Kelas } from './entities/kelas.entity';
import { KelasRepository } from './kelas.repository';

@Injectable()
export class KelasService {
  constructor(
    @InjectRepository(KelasRepository)
    private readonly kelasRepository: KelasRepository,
  ) {}

  async create(createKelaDto: CreateKelaDto): Promise<Kelas> {
    try {
      const result = await this.kelasRepository.save(createKelaDto);
      return { ...result, ...createKelaDto };
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async findAll(
    options: PaginationOptionsInterface,
  ): Promise<Pagination<Kelas>> {
    try {
      const results = await this.kelasRepository.find({
        where: {
          name: Like(`%${options.name}%`),
          is_deleted: false,
        },
        take: options.limit,
        skip: options.page * options.limit,
        order: { id: 'DESC' },
      });

      const total = await this.kelasRepository.count();

      return new Pagination<Kelas>({
        last_page: Math.ceil(total / options.limit),
        status: HttpStatus.OK,
        data: results,
        total: results.length,
        message: 'Success Get Kelas',
      });
    } catch (error) {
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  async findOne(id: number): Promise<Kelas> {
    try {
      const kelas = await this.kelasRepository.findOne(id, {
        where: {
          is_deleted: false,
        },
      });
      if (!kelas) {
        throw new HttpException('Data not found.', HttpStatus.NOT_FOUND);
      }
      return kelas;
    } catch (error) {
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  async update(id: number, updateKelaDto: UpdateKelaDto): Promise<Kelas> {
    try {
      const kelas = await this.kelasRepository.findOne(id);
      if (!kelas) {
        throw new HttpException('Data not found.', HttpStatus.NOT_FOUND);
      }
      const result = await this.kelasRepository.save({
        id,
        ...updateKelaDto,
      });
      const kelasUpdated = { ...kelas, ...result };
      return kelasUpdated;
    } catch (error) {
      // console.log(`error`, error);
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  async remove(id: number) {
    try {
      const { affected } = await getConnection()
        .createQueryBuilder()
        .update(Kelas)
        .set({ is_deleted: true })
        .where('id = :id', { id })
        .andWhere('is_deleted = :is_deleted', { is_deleted: false })
        .execute();

      if (!affected) {
        throw new HttpException('Data not found.', HttpStatus.NOT_FOUND);
      }

      throw new HttpException('Data Deleted.', HttpStatus.OK);
    } catch (error) {
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }
}
