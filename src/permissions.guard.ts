import {
  CanActivate,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';

@Injectable()
export class PermissionsGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const routePermissions = this.reflector.get<string>(
      'permissions',
      context.getHandler(),
    );
    // console.log(
    //   'context.getArgs()',
    //   context.getArgs()[0].user,
    //   routePermissions,
    // );
    const userPermissions = context.getArgs()[0].user.role;

    if (!routePermissions) {
      return true;
    }

    const hasPermission = () =>
      routePermissions[0].split(',').includes(userPermissions) ||
      userPermissions === 'Admin';
    if (!hasPermission()) {
      throw new HttpException('Forbidden Access', HttpStatus.FORBIDDEN);
    }
    return hasPermission();
  }
}
