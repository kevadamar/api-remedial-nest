import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpStatus,
  Req,
  UseInterceptors,
  UploadedFile,
  UseGuards,
} from '@nestjs/common';
import { RemedialService } from './remedial.service';
import { CreateRemedialDto } from './dto/create-remedial.dto';
import { UpdateRemedialDto } from './dto/update-remedial.dto';
import { Remedial } from './entities/remedial.entity';
import { Pagination } from 'src/pagination/pagination';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { JwtAuthGuard } from 'src/auth/jwt.auth.guard';
import { PermissionsGuard } from 'src/permissions.guard';
import { Permissions } from 'src/permissions.decorator';

@Controller('remedial')
export class RemedialController {
  constructor(private readonly remedialService: RemedialService) {}

  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @Permissions('Akademik,Mahasiswa')
  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './file_upload',
        filename: (_req, file, cb) => {
          cb(
            null,
            Date.now() + '_BuktiBayar-' + file.originalname.replace(/\s/g, ''),
          );
        },
      }),
    }),
  )
  async create(
    @Body() createRemedialDto: CreateRemedialDto,
    @UploadedFile() file: Express.Multer.File,
    @Req() request,
  ): Promise<object> {
    const data = await this.remedialService.create(
      { ...createRemedialDto, mhsId: request.user.id },
      file?.filename,
      file?.mimetype,
    );
    return {
      statusCode: HttpStatus.CREATED,
      message: 'Success Register Remedial',
      data,
    };
  }
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @Permissions('Dosen')
  @Get('/dosen')
  async findAllForDosen(@Req() request): Promise<Pagination<Remedial>> {
    return await this.remedialService.findAllForDosen(
      request.user,
      request.query.hasOwnProperty('flag_activity')
        ? request.query.flag_activity
        : 'UTS',
      request.query.hasOwnProperty('courseId') ? request.query.courseId : null,
    );
  }

  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @Permissions('Akademik')
  @Patch('/akademik/closing/:courseId/:flag_activity')
  async closingFlagOpen(
    @Param('courseId') courseId: string,
    @Param('flag_activity') flag_activity: string,
    @Req() req,
  ): Promise<object> {
    const data = await this.remedialService.closingFlagOpen(
      +courseId,
      flag_activity,
      req.user,
    );
    return {
      statusCode: HttpStatus.OK,
      message: 'Success Update',
      data,
    };
  }

  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @Permissions('Akademik')
  @Get('/akademik')
  async findAllForAkademik(@Req() request): Promise<Pagination<Remedial>> {
    return await this.remedialService.findAllForAkademik(
      request.query.hasOwnProperty('flag_activity')
        ? request.query.flag_activity
        : 'UTS',
      request.query.hasOwnProperty('courseId') ? request.query.courseId : null,
      request.query.hasOwnProperty('dosenId') ? request.query.dosenId : null,
    );
  }

  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @Permissions('Akademik,Mahasiswa')
  @Get()
  async findAll(@Req() request): Promise<Pagination<Remedial>> {
    return await this.remedialService.findAll(
      request.query.hasOwnProperty('type') ? request.query.type : 'default',
      request.user,
    );
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<object> {
    const data = await this.remedialService.findOne(+id);
    return {
      statusCode: HttpStatus.OK,
      message: 'Success Get',
      data,
    };
  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateRemedialDto: UpdateRemedialDto,
  ): Promise<object> {
    const data = await this.remedialService.update(+id, updateRemedialDto);
    return {
      statusCode: HttpStatus.OK,
      message: 'Success Update',
      data,
    };
  }

  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @Permissions('Akademik')
  @Patch(':id/approval')
  async approval(@Param('id') id: string, @Req() request): Promise<object> {
    const data = await this.remedialService.approval(
      +id,
      request.query.type,
      request.query?.remark,
      request.user,
    );
    return {
      statusCode: HttpStatus.OK,
      message: 'Success Update',
      data,
    };
  }

  @Patch(':id/mhs')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './file_upload',
        filename: (_req, file, cb) => {
          cb(
            null,
            Date.now() + '-jawaban-' + file.originalname.replace(/\s/g, ''),
          );
        },
      }),
    }),
  )
  async uploadJawaban(
    @Param('id') id: string,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<object> {
    const data = await this.remedialService.uploadFile(+id, 'jawaban', file);
    return {
      statusCode: HttpStatus.OK,
      message: 'Success Upload',
      data,
    };
  }

  @Patch(':id/dosen')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './file_upload',
        filename: (_req, file, cb) => {
          cb(
            null,
            Date.now() + '-soal-' + file.originalname.replace(/\s/g, ''),
          );
        },
      }),
    }),
  )
  async uploadSoal(
    @Param('id') id: string,
    @UploadedFile() file: Express.Multer.File,
    @Body() body,
  ): Promise<object> {
    const payload = {
      start: body.start,
      end: body.end,
      flag_activity: body.flag_activity,
    };
    const data = await this.remedialService.uploadFile(
      +id,
      'soal',
      file,
      payload,
    );
    return {
      statusCode: HttpStatus.OK,
      message: 'Success Upload',
      data,
    };
  }

  @Delete(':id')
  async remove(@Param('id') id: string): Promise<object> {
    const data = await this.remedialService.remove(+id);
    return {
      statusCode: HttpStatus.OK,
      message: 'Success Delete',
      data,
    };
  }
}
