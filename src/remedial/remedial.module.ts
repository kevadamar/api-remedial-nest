import { Module } from '@nestjs/common';
import { RemedialService } from './remedial.service';
import { RemedialController } from './remedial.controller';
import { Remedial } from './entities/remedial.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RemedialRepository } from './remedial.repository';
import { CloudinaryModule } from 'src/cloudinary/cloudinary.module';
import { AttachmentsModule } from 'src/attachments/attachments.module';

@Module({
  controllers: [RemedialController],
  providers: [RemedialService],
  imports: [
    TypeOrmModule.forFeature([RemedialRepository]),
    CloudinaryModule,
    AttachmentsModule,
  ],
})
export class RemedialModule {}
