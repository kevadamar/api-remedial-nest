import { EntityRepository, Repository } from 'typeorm';
import { Remedial } from './entities/remedial.entity';

@EntityRepository(Remedial)
export class RemedialRepository extends Repository<Remedial> {}
