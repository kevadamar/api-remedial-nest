import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AttachmentsService } from 'src/attachments/attachments.service';
import { CloudinaryService } from 'src/cloudinary/cloudinary.service';
import { Pagination } from 'src/pagination/pagination';
import { User } from 'src/users/entities/user.entity';
import { getConnection } from 'typeorm';
import { CreateRemedialDto } from './dto/create-remedial.dto';
import { UpdateRemedialDto } from './dto/update-remedial.dto';
import { Remedial } from './entities/remedial.entity';
import { RemedialRepository } from './remedial.repository';
import * as fs from 'fs';
import { pathImage } from 'src/helper';
@Injectable()
export class RemedialService {
  constructor(
    @InjectRepository(RemedialRepository)
    private readonly remedialRepository: RemedialRepository,
    private cloudinary: CloudinaryService,
    private attachment: AttachmentsService,
  ) {}

  async uploadImageToCloudinary(file: Express.Multer.File) {
    const resultUploadFile = file.filename;

    return resultUploadFile;
  }

  // create function closingFlagOpen for update flag_open to false
  async closingFlagOpen(
    id: number,
    flag_activity: string,
    user: User,
  ): Promise<boolean | undefined> {
    const remedial = await this.remedialRepository.findOne({
      where: { courseId: id, flag_activity, flag_open: true },
    });
    if (!remedial) {
      throw new HttpException(`Remedial not found.`, HttpStatus.NOT_FOUND);
    }
    await getConnection()
      .createQueryBuilder()
      .update(Remedial)
      .set({
        flag_open: false,
        closed_by: user.full_name,
      })
      .where('courseId = :id', { id })
      .andWhere('flag_activity = :flag_activity', {
        flag_activity,
      })
      .andWhere('flag_open = :flag_open', {
        flag_open: true,
      })
      .execute();
    return true;
  }

  async create(
    createRemedialDto: CreateRemedialDto,
    name?: string,
    type?: string,
    path?: string,
  ): Promise<Remedial> {
    try {
      let payload: any = {};
      // console.log(`createRemedialDto`, createRemedialDto);
      // find courseId by createRemedialDto.courseId from remedial
      const course = await this.remedialRepository.findOne({
        where: {
          courseId: createRemedialDto.courseId,
          semester: createRemedialDto.semester,
          flag_activity: createRemedialDto.flag_activity,
          isRejected: false,
          is_deleted: false,
          flag_open: true,
        },
        relations: ['course'],
      });
      // console.log('course 80', course);

      // validation if any data remedial has same
      const checkData = await this.remedialRepository.findOne({
        where: {
          mhsId: createRemedialDto.mhsId,
          courseId: createRemedialDto.courseId,
          semester: createRemedialDto.semester,
          flag_activity: createRemedialDto.flag_activity,
          isRejected: false,
          is_deleted: false,
          flag_open: true,
        },
        relations: ['course', 'course.dosen', 'paymentProof'],
      });
      if (checkData) {
        //handle if any file payment proof
        // console.log('checkData', checkData);
        if (name) {
          const currentImage = `${pathImage}${name}`;
          // console.log(`currtenImage`, currentImage);
          if (fs.existsSync(currentImage)) {
            fs.unlinkSync(currentImage);
          }
        }
        // if (checkData.end) {
        //   const expired =
        //     new Date().getTime() < new Date(checkData.end).getTime();

        //   if (!expired) {
        //     throw new HttpException(
        //       `Sorry registration time has run out.`,
        //       HttpStatus.NOT_FOUND,
        //     );
        //   }
        // }

        throw new HttpException(
          `You already registered in course ${createRemedialDto.flag_activity} ${checkData.course.name}.`,
          HttpStatus.NOT_FOUND,
        );
      }
      if (course?.end) {
        const expired = new Date().getTime() < new Date(course.end).getTime();
        // check if registration time has run out
        if (!expired) {
          throw new HttpException(
            `Sorry registration time has run out.`,
            HttpStatus.NOT_FOUND,
          );
        }
      }
      // save attachment payment proof
      const data = await this.attachment.create({
        name,
        type,
        path,
      });

      // save remedial
      payload = {
        ...payload,
        semester: createRemedialDto.semester,
        courseId: createRemedialDto.courseId,
        mhsId: createRemedialDto.mhsId,
        description_payment: createRemedialDto.description_payment,
        paymentProofId: data.id,
        flag_activity: createRemedialDto.flag_activity,
        start: course?.start ?? null,
        end: course?.end ?? null,
      };
      // if any data soal attachment id
      if (!course?.soalAttachmentId) {
      } else {
        payload = { ...payload, soalAttachmentId: course.soalAttachmentId };
      }
      const result = await this.remedialRepository.save(payload);
      const remedialUpdated = { ...result, ...createRemedialDto };
      return remedialUpdated;
    } catch (error) {
      console.log('error remedial create 155', error);
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  // find all remedial for akademik
  async findAllForAkademik(
    flag_activity: string,
    courseId?: number,
    dosenId?: number,
  ): Promise<Pagination<Remedial>> {
    let results = await this.remedialRepository.find({
      where: {
        flag_activity,
        isRejected: false,
        is_deleted: false,
        status: true,
        course: {
          id: courseId,
          dosenId,
        },
      },
      relations: [
        'course',
        'course.dosen',
        'mhs',
        'mhs.nilai',
        'jawabanAttachment',
        'soalAttachment',
      ],
      order: { id: 'DESC' },
    });
    console.log('courseId', flag_activity, results);
    results = results.map((res) => {
      const nilaiByCourse =
        res.mhs?.nilai.find(
          (n) =>
            n.courseId === res.courseId && n.flag_activity === flag_activity,
        ) ?? null;
      return {
        ...res,
        nilaiByCourse,
      };
    });

    return new Pagination<Remedial>({
      status: HttpStatus.OK,
      // data: results[0],
      // total: results[0].length,
      data: results,
      total: results.length,
      message: 'Success Get Detail Report Remedial',
    });
  }

  /**
   * dosenId by token dosen yang login
   * courseId by query param -> untuk lihat list nilai by course
   */
  async findAllForDosen(
    user: User,
    flag_activity: string,
    courseId?: number,
  ): Promise<Pagination<Remedial>> {
    try {
      let results = [];

      let message = 'Success Get Remedial Dosen';

      if (courseId) {
        // results = await this.remedialRepository.query(
        //   `CALL sp_list_score_by_dosen(${user.id},${courseId})`,
        // );

        results = await this.remedialRepository.find({
          where: {
            flag_activity: flag_activity,
            isRejected: false,
            is_deleted: false,
            status: true,
            course: {
              id: courseId,
              dosenId: user.id,
            },
          },
          relations: [
            'course',
            'course.dosen',
            'mhs',
            'mhs.nilai',
            'jawabanAttachment',
            'soalAttachment',
          ],
          order: { id: 'DESC' },
        });
        console.log('courseId', flag_activity, results);
        results = results.map((res) => {
          const nilaiByCourse =
            res.mhs?.nilai.find(
              (n) =>
                n.courseId === res.courseId &&
                n.flag_activity === flag_activity,
            ) ?? null;
          return {
            ...res,
            nilaiByCourse,
          };
        });

        message = 'Success Get Detial List Participant Remedial By Dosen';
      } else {
        // results = await this.remedialRepository.query(
        //   `CALL sp_remedial_for_dosen(${user.id})`,
        // );
        results = await this.remedialRepository.query(
          `SELECT remedial.id AS remedial_id,
          mhs.id AS mhs_id,
          soalAttachment.id AS soalAttachmentId,
          soalAttachment.name AS name_soal,
          soalAttachment.path AS path_soal,
          remedial.flag_open AS flag_open,
          remedial.start AS start,
          remedial.end AS end,
          remedial.flag_activity AS flag_activity,
          remedial.semester AS semester,
          remedial.is_deleted AS is_deleted,
          remedial.status AS status,
          remedial.isRejected AS isRejected,
          remedial.approved_by AS approved_by,
          remedial.courseId AS courseId,
          remedial.updated_at AS updated_at,
          remedial.created_at AS created_at,
          course.id AS course_id,
          course.code AS course_code,
          course.name AS course_name,
          course.sks AS sks,
          course.kurikulum_semester AS kurikulum_semester,
          course.dosenId AS course_dosenId,
          dosen.id AS dosen_id,
          dosen.full_name AS dosen_full_name,
          dosen.email AS dosen_email,
          dosen.nim AS dosen_nim,
          dosen.userDetailId AS dosen_userDetailId,
          (
              SELECT COUNT(*) AS cid
              FROM remedial remedial2
              WHERE remedial2.courseId = remedial.courseId AND remedial2.is_deleted = false AND remedial2.status = true AND remedial2.isRejected = false AND remedial2.flag_activity = remedial.flag_activity AND remedial2.flag_open = true
          ) AS totalRemedial
      FROM remedial remedial
          LEFT JOIN attachment soalAttachment ON soalAttachment.id = remedial.soalAttachmentId
          LEFT JOIN course course ON course.id = remedial.courseId
          LEFT JOIN user mhs ON mhs.id = remedial.mhsId
          LEFT JOIN kelas kelas ON kelas.id = mhs.kelasId
          LEFT JOIN user dosen ON dosen.id = course.dosenId
      WHERE remedial.is_deleted = false
          AND remedial.status = true
          AND dosen.id = ${user.id} AND remedial.flag_open = true
      GROUP BY remedial.courseId,course.kurikulum_semester,remedial.flag_activity
      ORDER BY remedial.created_at DESC`,
        );
        // results = results[0];
      }

      return new Pagination<Remedial>({
        status: HttpStatus.OK,
        // data: results[0],
        // total: results[0].length,
        data: results,
        total: results.length,
        message,
      });
    } catch (error) {
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  /**
   * type == 'default' untuk menu approval remedial -> akademik
   * type == 'report' untuk menu report remedial buat print excel -> akademik
   * type == 'mhs' untuk menu remedial buat cek semua remedial dia by token / id -> mahasiswa
   */
  async findAll(type: string, user?: User): Promise<Pagination<Remedial>> {
    try {
      let results = null;

      if (type === 'report') {
        results = await this.remedialRepository.query(
          `SELECT
          course.id AS course_id,
          course.code AS course_code,
          course.name AS course_name,
          course.sks AS sks,
          course.kurikulum_semester AS kurikulum_semester,
          Remedial.flag_open AS flag_open,
          Remedial__course.name AS Remedial__course_name,
          Remedial__course__dosen.full_name AS Remedial__course__dosen_full_name,
          Remedial.id AS Remedial_id,
          Remedial.courseId AS Remedial_courseId,
          (
          SELECT
              COUNT(*) AS cid
          FROM
              remedial remedial2
          WHERE
              remedial2.courseId = Remedial.courseId AND remedial2.isRejected = FALSE AND remedial2.status = TRUE AND remedial2.flag_activity = Remedial.flag_activity
      ) AS totalRemedial,
          (
          SELECT
              COUNT(*) AS nid
          FROM
              nilai n
          WHERE
      Remedial.courseId = n.courseId AND Remedial.flag_activity = n.flag_activity AND n.is_deleted = FALSE
      ) AS totalNilaiFilled,
          Remedial.semester AS Remedial_semester,
      Remedial.is_deleted AS Remedial_is_deleted,
      Remedial.status AS Remedial_status,
      Remedial.isRejected AS Remedial_isRejected,
      Remedial.approved_by AS Remedial_approved_by,
      Remedial.updated_at AS Remedial_updated_at,
      Remedial.created_at AS Remedial_created_at,
      Remedial.flag_activity AS Remedial_flag_activity,
      Remedial__soalAttachment.id AS Remedial__soalAttachment_id,
      Remedial__soalAttachment.name AS Remedial__soalAttachment_name,
      Remedial__soalAttachment.path AS Remedial__soalAttachment_path,
      Remedial__jawabanAttachment.id AS Remedial__jawabanAttachment_id,
      Remedial__jawabanAttachment.name AS Remedial__jawabanAttachment_name,
      Remedial__jawabanAttachment.path AS Remedial__jawabanAttachment_path,
      Remedial__paymentProof.id AS Remedial__paymentProof_id,
      Remedial__paymentProof.name AS Remedial__paymentProof_name,
      Remedial__paymentProof.path AS Remedial__paymentProof_path,
      Remedial__mhs.id AS Remedial__mhs_id,
      Remedial__mhs.full_name AS Remedial__mhs_full_name,
      Remedial__mhs.email AS Remedial__mhs_email,
      Remedial__mhs.username AS Remedial__mhs_username,
      Remedial__mhs.nim AS Remedial__mhs_nim,
      Remedial__mhs.userDetailId AS Remedial__mhs_userDetailId,
      Remedial__course.id AS Remedial__course_id,
      Remedial__course.code AS Remedial__course_code,
      Remedial__course.sks AS sks,
      Remedial__course.is_deleted AS Remedial__course_is_deleted,
      Remedial__course.dosenId AS Remedial__course_dosenId,
      Remedial__course__dosen.id AS Remedial__course__dosen_id,
      Remedial__course__dosen.email AS Remedial__course__dosen_email,
      Remedial__course__dosen.nim AS Remedial__course__dosen_nim
      FROM
          remedial Remedial
      LEFT JOIN course course ON course.id = Remedial.courseId
      LEFT JOIN attachment Remedial__soalAttachment ON
          Remedial__soalAttachment.id = Remedial.soalAttachmentId
      LEFT JOIN attachment Remedial__jawabanAttachment ON
          Remedial__jawabanAttachment.id = Remedial.jawabanAttachmentId
      LEFT JOIN attachment Remedial__paymentProof ON
          Remedial__paymentProof.id = Remedial.paymentProofId
      LEFT JOIN user Remedial__mhs ON
          Remedial__mhs.id = Remedial.mhsId
      LEFT JOIN course Remedial__course ON
          Remedial__course.id = Remedial.courseId
      LEFT JOIN user Remedial__course__dosen ON
          Remedial__course__dosen.id = Remedial__course.dosenId
      WHERE
          (
              Remedial.is_deleted = FALSE AND Remedial.status = TRUE AND NOT(Remedial.approved_by IS NULL) AND Remedial.isRejected = FALSE -- AND Remedial.flag_open
          )
      GROUP BY
          Remedial.courseId,course.kurikulum_semester,Remedial.flag_activity
      ORDER BY
          Remedial.created_at
      DESC`,
        );
        // results = results[0];
      } else if (type === 'mhs') {
        results = await this.remedialRepository.find({
          where: {
            is_deleted: false,
            mhs: {
              id: user.id,
            },
          },
          relations: [
            'soalAttachment',
            'jawabanAttachment',
            'paymentProof',
            'mhs',
            'mhs.kelas',
            'mhs.nilai',
            'course',
            'course.dosen',
          ],
          order: { created_at: 'DESC' },
        });

        results = results.map((res) => {
          const nilaiByCourse =
            res.mhs?.nilai.find(
              (n) =>
                n.courseId === res.courseId &&
                n.flag_activity === res.flag_activity,
            ) ?? null;
          return {
            ...res,
            nilaiByCourse,
          };
        });
      } else {
        results = await this.remedialRepository.find({
          where: {
            is_deleted: false,
            // status: false,
            // approved_by: null,
            // mhs: {
            //   full_name: Like('%ananta%'),
            // },
          },
          relations: [
            'soalAttachment',
            'jawabanAttachment',
            'paymentProof',
            'mhs',
            'mhs.kelas',
            'course',
            'course.dosen',
          ],
          order: { created_at: 'DESC' },
        });
      }

      return new Pagination<Remedial>({
        status: HttpStatus.OK,
        data: results,
        total: results.length,
        message: 'Success Get Remedial',
      });
    } catch (error) {
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  async findOne(id: number): Promise<Remedial> {
    try {
      const remedial = await this.remedialRepository.findOne(id, {
        relations: ['soalAttachment', 'mhs', 'course', 'course.dosen'],
      });
      if (!remedial) {
        throw new HttpException('Data not found.', HttpStatus.NOT_FOUND);
      }
      return remedial;
    } catch (error) {
      // console.log(`error`, error);
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  /**
   * id = adalah id course jika tipe nya soal, jika tipenya jawaban makan id akan menjadi remedial id
   * type = flag tipe jawaban atau soal
   */
  async uploadFile(
    id: number,
    type: string,
    file?: Express.Multer.File,
    payload?: any,
  ): Promise<boolean | undefined> {
    try {
      if (type.toLowerCase() === 'jawaban') {
        const remedial = await this.remedialRepository.findOne({
          where: {
            id,
          },
          relations: ['jawabanAttachment'],
        });
        if (remedial) {
          const idAttchment = remedial.jawabanAttachment?.id;
          console.log('attchment', idAttchment);
          if (!idAttchment) {
            // save attachment
            const data = await this.attachment.create({
              name: file.filename,
              type: file.mimetype,
            });
            await this.remedialRepository.save({
              id,
              jawabanAttachmentId: data.id,
            });
            return true;
          }
          // update attachment
          await this.attachment.update(
            idAttchment,
            file.filename,
            file.mimetype,
          );
          // await this.remedialRepository.save({
          //   id,
          //   jawabanAttachmentId: data.id,
          // });
          return true;
        }

        throw new HttpException('Data Not Found.', HttpStatus.NOT_FOUND);
      } else {
        const remedial = await this.remedialRepository.find({
          where: {
            courseId: id,
            flag_activity: payload.flag_activity,
            flag_open: true,
          },
          relations: ['soalAttachment'],
        });

        if (!remedial.length) {
          const idAttchment = remedial[0].soalAttachment?.id;
          if (!idAttchment) {
            // save attachment
            const data = await this.attachment.create({
              name: file.filename,
              type: file.mimetype,
            });
            // await this.remedialRepository.save({
            //   id,
            //   soalAttachmentId: data.id,
            //   ...payload,
            // });
            console.log('masuk sni 571');
            await getConnection()
              .createQueryBuilder()
              .update(Remedial)
              .set({
                start: payload.start,
                end: payload.end,
                soalAttachmentId: data.id,
              })
              .where('courseId = :id', { id })
              .andWhere('flag_activity = :flag_activity', {
                flag_activity: payload.flag_activity,
              })
              .andWhere('flag_open = :flag_open', {
                flag_open: true,
              })
              .execute();
            return true;
          }
          // update attachment
          if (file) {
            await this.attachment.update(
              idAttchment,
              file.filename,
              file.mimetype,
            );
          }
          console.log('masuk sni 595');
          Promise.all(
            remedial?.map((remed) => {
              getConnection()
                .createQueryBuilder()
                .update(Remedial)
                .set({
                  start: payload.start,
                  end: payload.end,
                })
                .where('id = :id', { id: remed.id })
                .andWhere('flag_activity = :flag_activity', {
                  flag_activity: payload.flag_activity,
                })
                .andWhere('flag_open = :flag_open', {
                  flag_open: true,
                })
                .execute();
              // this.remedialRepository.save({
              //   id: remed.id,
              //   ...payload,
              // });
            }),
          ).then(() => {
            return true;
          });
        }

        const idAttchment = remedial[0].soalAttachment?.id;
        if (!idAttchment) {
          console.log('debugremedial 622');
          // save attachment
          const data = await this.attachment.create({
            name: file.filename,
            type: file.mimetype,
          });
          Promise.all(
            remedial?.map((remed) => {
              // this.remedialRepository.save({
              //   id: remed.id,
              //   soalAttachmentId: data.id,
              //   ...payload,
              // });
              getConnection()
                .createQueryBuilder()
                .update(Remedial)
                .set({
                  soalAttachmentId: data.id,
                  ...payload,
                })
                .where('id = :id', { id: remed.id })
                .andWhere('flag_activity = :flag_activity', {
                  flag_activity: payload.flag_activity,
                })
                .andWhere('flag_open = :flag_open', {
                  flag_open: true,
                })
                .execute();
            }),
          );
          return true;
        }
        // update attachment
        if (file) {
          await this.attachment.update(
            idAttchment,
            file.filename,
            file.mimetype,
          );
        }
        console.log('masuk sni 659');
        Promise.all(
          remedial?.map((remed) => {
            getConnection()
              .createQueryBuilder()
              .update(Remedial)
              .set({
                start: payload.start,
                end: payload.end,
              })
              .where('id = :id', { id: remed.id })
              .andWhere('flag_activity = :flag_activity', {
                flag_activity: payload.flag_activity,
              })
              .andWhere('flag_open = :flag_open', {
                flag_open: true,
              })
              .execute();
            // this.remedialRepository.save({
            //   id: remed.id,
            //   ...payload,
            // });
          }),
        );
        return true;
      }
      return true;
    } catch (error) {
      console.log(`error`, error);
      const name = file.filename;
      if (name) {
        const currentImage = `${pathImage}${name}`;
        // console.log(`currtenImage`, currentImage);
        if (fs.existsSync(currentImage)) {
          fs.unlinkSync(currentImage);
        }
      }
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  /**
   * type == 'a' untuk menu approve dan merubah status
   * type == 'r' untuk menu rejected dan memberikan remark
   */
  async approval(
    id: number,
    type: string,
    remark?: string,
    user?: User,
  ): Promise<boolean | undefined> {
    try {
      const remedial = await this.remedialRepository.findOne(id);
      if (!remedial) {
        throw new HttpException('Data not found.', HttpStatus.NOT_FOUND);
      }
      console.log('remedial', remedial, type);
      if (type.toLowerCase() === 'a') {
        this.remedialRepository.save({
          id: remedial.id,
          status: true,
          approved_by: user.full_name,
        });
      } else {
        this.remedialRepository.save({
          id: remedial.id,
          isRejected: true,
          remark,
          approved_by: user.full_name,
        });
      }

      return true;
    } catch (error) {
      // console.log(`error`, error);
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  async update(
    id: number,
    updateRemedialDto: UpdateRemedialDto,
  ): Promise<Remedial> {
    try {
      const remedial = await this.remedialRepository.findOne(id);
      if (!remedial) {
        throw new HttpException('Data not found.', HttpStatus.NOT_FOUND);
      }
      const result = await this.remedialRepository.save({
        id,
        ...updateRemedialDto,
      });
      const remedialUpdated = { ...remedial, ...result };
      return remedialUpdated;
    } catch (error) {
      // console.log(`error`, error);
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  async remove(id: number) {
    try {
      const { affected } = await getConnection()
        .createQueryBuilder()
        .update(Remedial)
        .set({ is_deleted: true })
        .where('id = :id', { id })
        .andWhere('is_deleted = :is_deleted', { is_deleted: false })
        .execute();

      if (!affected) {
        throw new HttpException('Data not found.', HttpStatus.NOT_FOUND);
      }

      throw new HttpException('Data Deleted.', HttpStatus.OK);
    } catch (error) {
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }
}
