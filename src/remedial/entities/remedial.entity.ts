import { Attachment } from 'src/attachments/entities/attachment.entity';
import { Course } from 'src/courses/entities/course.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('remedial')
export class Remedial {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ default: null })
  semester: number;

  @Column({ default: false })
  is_deleted: boolean;

  @Column({ default: null })
  description_payment: string;

  @Column({ type: 'timestamp', nullable: true })
  start: Date;
  @Column({ type: 'timestamp', nullable: true })
  end: Date;

  @Column()
  flag_activity: string;

  @Column({ default: true })
  flag_open: boolean;

  @Column({ default: false })
  status: boolean;

  @Column({ default: false })
  isRejected: boolean;

  @Column({ nullable: true })
  remark: string;

  @Column({ nullable: true })
  approved_by: string;

  @Column({ nullable: true })
  closed_by: string;

  //   fk

  @Column({ default: null })
  mhsId: number;

  @Column({ default: null })
  courseId: number;

  @Column({ default: null })
  soalAttachmentId: number;

  @Column({ default: null })
  jawabanAttachmentId: number;

  @Column({ default: null })
  paymentProofId: number;

  @Column({ type: 'timestamp' })
  updated_at: Date;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  created_at: Date;

  //   relations
  @ManyToOne(() => User, (user) => user.course)
  @JoinColumn()
  mhs: User;

  @ManyToOne(() => Course, (course) => course.remedial)
  @JoinColumn()
  course: Course;

  @ManyToOne(() => Attachment, (attachment) => attachment.soalRemedial)
  @JoinColumn()
  soalAttachment: Attachment;

  @ManyToOne(() => Attachment, (attachment) => attachment.jawabanRemedial)
  @JoinColumn()
  jawabanAttachment: Attachment;

  @ManyToOne(() => Attachment, (attachment) => attachment.paymentProofRemedial)
  @JoinColumn()
  paymentProof: Attachment;
}
