import { Transform } from 'class-transformer';
import { IsInt, IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class CreateRemedialDto {
  @IsNotEmpty()
  @Transform((value) => (isNaN(+value.value) ? 0 : +value?.value))
  @IsInt()
  semester: number;
  @IsNotEmpty()
  @Transform((value) => (isNaN(+value.value) ? 0 : +value?.value))
  @IsInt()
  courseId?: number;
  @IsOptional()
  @Transform((value) => (isNaN(+value.value) ? 0 : +value?.value))
  @IsInt()
  mhsId?: number;
  @IsString()
  description_payment: string;
  @IsString()
  @IsNotEmpty()
  flag_activity: string;
  // @IsNumber()
  // soalAttachmentId?: number;
  // @IsNumber()
  // jawabanAttachmentId?: number;
}
