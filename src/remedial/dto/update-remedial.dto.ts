import { PartialType } from '@nestjs/mapped-types';
import { CreateRemedialDto } from './create-remedial.dto';

export class UpdateRemedialDto extends PartialType(CreateRemedialDto) {}
