import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { NilaiModule } from './nilai/nilai.module';
import { UsersModule } from './users/users.module';
import { CoursesModule } from './courses/courses.module';
import { RemedialModule } from './remedial/remedial.module';
import { KelasModule } from './kelas/kelas.module';
import { PaymentProofModule } from './payment-proof/payment-proof.module';
import { AttachmentsModule } from './attachments/attachments.module';
import { ExcelFileModule } from './excel-file/excel-file.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RoleModule } from './role/role.module';
import { ProdiModule } from './prodi/prodi.module';
import { UserDetailModule } from './user-detail/user-detail.module';
import { AuthModule } from './auth/auth.module';
import { CloudinaryModule } from './cloudinary/cloudinary.module';

@Module({
  imports: [
    NilaiModule,
    UsersModule,
    CoursesModule,
    RemedialModule,
    KelasModule,
    PaymentProofModule,
    AttachmentsModule,
    ExcelFileModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: '103.226.138.168', //<-- dev
      port: 3306, //<-- dev
      username: 'userpublic', //<-- dev
      password: '@Kadalgurun18', //<-- dev
      database: 'remedial_app',
      // host: '127.0.0.1',
      // port: 8889,
      // username: 'root',
      // password: 'root',
      // database: 'remedial_app',
      autoLoadEntities: true,
      synchronize: true,
      logging: true,
    }),
    RoleModule,
    ProdiModule,
    UserDetailModule,
    AuthModule,
    CloudinaryModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
