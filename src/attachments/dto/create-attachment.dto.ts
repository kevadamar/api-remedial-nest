import { IsString } from 'class-validator';

export class CreateAttachmentDto {
  @IsString()
  name: string;
  @IsString()
  type: string;
  @IsString()
  path?: string;
}
