import { Remedial } from 'src/remedial/entities/remedial.entity';
import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('attachment')
export class Attachment {
  @PrimaryGeneratedColumn('increment')
  id: number;
  @Column()
  name: string;
  @Column()
  type: string;
  @Column({ nullable: true })
  path: string;
  @Column({ type: 'timestamp' })
  updated_at: Date;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  created_at: Date;

  //   relations
  @OneToMany(() => Remedial, (remedial) => remedial.soalAttachment)
  @JoinColumn()
  soalRemedial: Remedial[];

  @OneToMany(() => Remedial, (remedial) => remedial.jawabanAttachment)
  @JoinColumn()
  jawabanRemedial: Remedial[];

  @OneToMany(() => Remedial, (remedial) => remedial.paymentProof)
  @JoinColumn()
  paymentProofRemedial: Remedial[];
}
