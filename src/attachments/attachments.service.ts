import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AttachmentRepository } from './attachments.repository';
import { CreateAttachmentDto } from './dto/create-attachment.dto';
import { UpdateAttachmentDto } from './dto/update-attachment.dto';
import { Attachment } from './entities/attachment.entity';

@Injectable()
export class AttachmentsService {
  constructor(
    @InjectRepository(AttachmentRepository)
    private readonly attachmentRepository: AttachmentRepository,
  ) {}
  async create(createAttachmentDto: CreateAttachmentDto): Promise<Attachment> {
    try {
      const data = await this.attachmentRepository.save({
        ...createAttachmentDto,
      });
      return data;
    } catch (error) {
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  findAll() {
    return `This action returns all attachments`;
  }

  findOne(id: number) {
    return `This action returns a #${id} attachment`;
  }

  async update(id: number, name: string, type: string): Promise<void> {
    try {
      await this.attachmentRepository.save({
        id,
        name,
        type,
      });
    } catch (error) {
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  remove(id: number) {
    return `This action removes a #${id} attachment`;
  }
}
