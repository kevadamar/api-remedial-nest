import { Module } from '@nestjs/common';
import { AttachmentsService } from './attachments.service';
import { AttachmentsController } from './attachments.controller';
import { Attachment } from './entities/attachment.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AttachmentRepository } from './attachments.repository';

@Module({
  controllers: [AttachmentsController],
  providers: [AttachmentsService],
  imports: [TypeOrmModule.forFeature([AttachmentRepository])],
  exports: [AttachmentsService],
})
export class AttachmentsModule {}
