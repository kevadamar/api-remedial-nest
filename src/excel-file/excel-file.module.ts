import { Module } from '@nestjs/common';
import { ExcelFileService } from './excel-file.service';
import { ExcelFileController } from './excel-file.controller';
import { NilaiModule } from 'src/nilai/nilai.module';

@Module({
  imports: [NilaiModule],
  controllers: [ExcelFileController],
  providers: [ExcelFileService],
})
export class ExcelFileModule {}
