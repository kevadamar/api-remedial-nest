import { Injectable } from '@nestjs/common';
import { CreateExcelFileDto } from './dto/create-excel-file.dto';
import { UpdateExcelFileDto } from './dto/update-excel-file.dto';

@Injectable()
export class ExcelFileService {
  create(createExcelFileDto: CreateExcelFileDto) {
    return 'This action adds a new excelFile';
  }

  findAll() {
    return `This action returns all excelFile`;
  }

  findOne(id: number) {
    return `This action returns a #${id} excelFile`;
  }

  update(id: number, updateExcelFileDto: UpdateExcelFileDto) {
    return `This action updates a #${id} excelFile`;
  }

  remove(id: number) {
    return `This action removes a #${id} excelFile`;
  }
}
