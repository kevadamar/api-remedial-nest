import { PartialType } from '@nestjs/mapped-types';
import { CreateExcelFileDto } from './create-excel-file.dto';

export class UpdateExcelFileDto extends PartialType(CreateExcelFileDto) {}
