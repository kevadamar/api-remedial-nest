import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpStatus,
  HttpCode,
  Header,
  Res,
  Req,
  HttpException,
} from '@nestjs/common';
import { Response as ExpressResponse } from 'express';
import { ExcelFileService } from './excel-file.service';
import { CreateExcelFileDto } from './dto/create-excel-file.dto';
import { UpdateExcelFileDto } from './dto/update-excel-file.dto';
import * as Exceljs from 'exceljs';
import { NilaiService } from 'src/nilai/nilai.service';
import { Nilai } from 'src/nilai/entities/nilai.entity';

@Controller('excel')
export class ExcelFileController {
  constructor(
    private readonly excelFileService: ExcelFileService,
    private readonly nilaiService: NilaiService,
  ) {}

  @Get('download')
  @HttpCode(HttpStatus.OK)
  @Header('Content-Type', 'text/xlsx')
  @Header('filename', 'komponen_nilai.xlsx')
  async downloadExcel(
    @Res() res: ExpressResponse,
    @Req() request,
  ): Promise<ExpressResponse> {
    try {
      const dosenId = request.query.hasOwnProperty('dosenId');
      const courseId = request.query.hasOwnProperty('courseId');
      const semester = request.query.hasOwnProperty('semester');

      if (!dosenId) {
        throw new HttpException(
          'Param Dosen Harus diisi!',
          HttpStatus.BAD_REQUEST,
        );
      }

      if (!courseId) {
        throw new HttpException(
          'Param Course Harus diisi!',
          HttpStatus.BAD_REQUEST,
        );
      }

      if (!semester) {
        throw new HttpException(
          'Param Semester Harus diisi!',
          HttpStatus.BAD_REQUEST,
        );
      }

      const workbook = new Exceljs.Workbook();
      const alphabet = [...'ABCDE'];

      // write to a stream
      const worksheet = workbook.addWorksheet('KOMPONEN NILAI', {
        views: [{ showGridLines: false }],
        headerFooter: {
          firstHeader: 'Keva',
          firstFooter: 'Mantapp',
        },
      });
      // headers excel table
      let dosen = '',
        matkul = '',
        kurikulum = '',
        sks = '';
      // let nilai: any = await this.nilaiService.findAll(2, 1, 3);
      let nilai: any = await this.nilaiService.findAll(
        parseInt(request.query.dosenId),
        parseInt(request.query.courseId),
        parseInt(request.query.semester),
        request.query.flag_activity,
      );
      console.log(`nilai`, nilai);

      if (nilai.length === 0) {
        const object = [
          request.query.dosenId,
          request.query.courseId,
          request.query.semester,
        ];
        console.log(`object`, object);
        throw new HttpException('Nilai Tidak Ditemukan!', HttpStatus.NOT_FOUND);
      }
      nilai = nilai.map((n, i) => {
        if (i === 0) {
          dosen = n.course.dosen.full_name;
          matkul = n.course.name;
          sks = n.course.sks;
          kurikulum = n.course.kurikulum_semester;
        }
        return {
          ['B']: n.mhs?.kelas?.name,
          ['C']: n.mhs?.nim,
          ['D']: n.mhs?.full_name,
          // ['E']: n.presense,
          // ['F']: n.task,
          // ['G']: n.formative,
          // ['H']: n.behavior,
          // ['I']: n.mid_test,
          // ['J']: n.full_test,
          ['E']: n.total,
          // ['F']: n.task,
          // ['L']: n.grade,
        };
      });

      worksheet.mergeCells('A1:L1');
      worksheet.getCell('A1').value = 'HASIL PENILAIAN REMEDIAL';
      worksheet.getCell('A1').alignment = { horizontal: 'center' };
      worksheet.getCell('A1').font = { name: 'Arial', size: 18, bold: true };

      worksheet.mergeCells('A2:L2');
      worksheet.getCell('A2').value =
        'INSTITUT TEKNOLOGI & BISNIS BINA SARANA GLOBAL';
      worksheet.getCell('A2').alignment = { horizontal: 'center' };
      worksheet.getCell('A2').font = { name: 'Arial', size: 14, bold: true };

      const yearnow = new Date().getFullYear();
      worksheet.mergeCells('A3:L3');
      worksheet.getCell('A3').value = `TAHUN AKADEMIK ${yearnow} - ${
        yearnow + 1
      }`;
      worksheet.getCell('A3').alignment = { horizontal: 'center' };
      worksheet.getCell('A3').font = { name: 'Arial', size: 11 };

      worksheet.getCell('A4').value = 'Nama Dosen';
      worksheet.getCell('A4').font = { name: 'Arial', size: 12 };
      worksheet.getCell('C4').value = `: ${dosen}`;
      worksheet.getCell('C4').font = { name: 'Arial', size: 12 };

      worksheet.getCell('A5').value = 'Mata Kuliah / SKS';
      worksheet.getCell('A5').font = { name: 'Arial', size: 12 };

      worksheet.getCell('C5').value = `: ${matkul} / ${sks}`;
      worksheet.getCell('C5').font = { name: 'Arial', size: 12 };

      worksheet.getCell('A6').value = 'Semester ';
      worksheet.getCell('A6').font = { name: 'Arial', size: 12 };

      worksheet.getCell('C6').value = `: ${request.query.semester}`;
      worksheet.getCell('C6').font = { name: 'Arial', size: 12 };

      worksheet.getCell('A7').value = 'Kurikulum ';
      worksheet.getCell('A7').font = { name: 'Arial', size: 12 };

      worksheet.getCell('C7').value = `: ${kurikulum}`;
      worksheet.getCell('C7').font = { name: 'Arial', size: 12 };

      // header start
      worksheet.mergeCells('A9');
      worksheet.getCell('A9').value = 'No';
      worksheet.getCell('A9').alignment = {
        vertical: 'middle',
        horizontal: 'center',
      };
      worksheet.getCell('A9').font = { name: 'Arial', size: 11, bold: true };
      worksheet.getCell('A9').fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'DEE6F0' },
      };
      worksheet.getCell('A9').border = {
        top: { style: 'thin' },
        left: { style: 'thin' },
        bottom: { style: 'thin' },
        right: { style: 'thin' },
      };

      worksheet.mergeCells('B9');
      worksheet.getCell('B9').value = 'Kelas';
      worksheet.getCell('B9').alignment = {
        vertical: 'middle',
        horizontal: 'center',
      };
      worksheet.getCell('B9').font = { name: 'Arial', size: 11, bold: true };
      worksheet.getCell('B9').fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'DEE6F0' },
      };
      worksheet.getCell('B9').border = {
        top: { style: 'thin' },
        left: { style: 'thin' },
        bottom: { style: 'thin' },
        right: { style: 'thin' },
      };

      worksheet.mergeCells('C9');
      worksheet.getCell('C9').value = 'NPM';
      worksheet.getCell('C9').alignment = {
        vertical: 'middle',
        horizontal: 'center',
      };
      worksheet.getCell('C9').font = { name: 'Arial', size: 11, bold: true };
      worksheet.getCell('C9').fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'DEE6F0' },
      };
      worksheet.getCell('C9').border = {
        top: { style: 'thin' },
        left: { style: 'thin' },
        bottom: { style: 'thin' },
        right: { style: 'thin' },
      };

      worksheet.mergeCells('D9');
      worksheet.getCell('D9').value = 'Nama Mahasiswa';

      worksheet.getCell('D9').alignment = {
        vertical: 'middle',
        horizontal: 'center',
      };
      worksheet.getCell('D9').font = { name: 'Arial', size: 11, bold: true };
      worksheet.getCell('D9').fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'DEE6F0' },
      };
      worksheet.getCell('D9').border = {
        top: { style: 'thin' },
        left: { style: 'thin' },
        bottom: { style: 'thin' },
        right: { style: 'thin' },
      };

      worksheet.getCell('E9').value = 'TOTAL';
      worksheet.getCell('E9').alignment = {
        vertical: 'middle',
        horizontal: 'center',
      };
      worksheet.getCell('E9').font = { name: 'Arial', size: 11, bold: true };
      worksheet.getCell('E9').fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'DEE6F0' },
      };
      worksheet.getCell('E9').border = {
        top: { style: 'thin' },
        left: { style: 'thin' },
        bottom: { style: 'thin' },
        right: { style: 'thin' },
      };

      // worksheet.mergeCells('L8:L9');
      // worksheet.getCell('L8').value = 'Grade';

      // worksheet.getCell('L8').alignment = {
      //   vertical: 'middle',
      //   horizontal: 'center',
      // };
      // worksheet.getCell('L8').font = { name: 'Arial', size: 11, bold: true };
      // worksheet.getCell('L8').fill = {
      //   type: 'pattern',
      //   pattern: 'solid',
      //   fgColor: { argb: 'DEE6F0' },
      // };
      // worksheet.getCell('L8').border = {
      //   top: { style: 'thin' },
      //   left: { style: 'thin' },
      //   bottom: { style: 'thin' },
      //   right: { style: 'thin' },
      // };

      // set width column
      worksheet.getRow(8).worksheet.columns.forEach(function (column, i) {
        let maxLength = 0;
        if (i === 3) {
          column['eachCell']({ includeEmpty: true }, function (cell) {
            const columnLength = cell.value ? cell.value.toString().length : 10;
            if (columnLength > maxLength) {
              maxLength = columnLength;
            }
          });
          column.width = 37;
        } else if (i > 3) {
          column.width = 10.3;
        } else if (i === 1) {
          column.width = 13.3;
        } else if (i === 2) {
          column.width = 13.3;
        }
      });

      // console.log(`nilai`, nilai);

      nilai.map((score, index) => {
        alphabet.map((alpa, idx) => {
          if (idx === 0) {
            worksheet.getCell(`${alpa}${index + 10}`).value = index + 1;
            worksheet.getCell(`${alpa}${index + 10}`).font = {
              name: 'Arial',
              size: 11,
              bold: true,
            };
            worksheet.getCell(`${alpa}${index + 10}`).alignment = {
              horizontal: 'center',
            };
            worksheet.getCell(`${alpa}${index + 10}`).border = {
              top: { style: 'thin' },
              left: { style: 'thin' },
              bottom: { style: 'thin' },
              right: { style: 'thin' },
            };
          } else {
            worksheet.getCell(`${alpa}${index + 10}`).value =
              score[alpa].toString();
            worksheet.getCell(`${alpa}${index + 10}`).font = {
              name: 'Arial',
              size: 11,
              bold: true,
            };
            worksheet.getCell(`${alpa}${index + 10}`).border = {
              top: { style: 'thin' },
              left: { style: 'thin' },
              bottom: { style: 'thin' },
              right: { style: 'thin' },
            };
            if (idx > 3) {
              worksheet.getCell(`${alpa}${index + 10}`).alignment = {
                horizontal: 'center',
              };
            }
          }
        });
      });

      worksheet.getCell(`G${nilai.length + 15 + 2}`).value =
        'Tangerang,  ________________';

      worksheet.getCell(`G${nilai.length + 15 + 2}`).font = {
        name: 'Arial',
        size: 11,
        bold: true,
      };

      // worksheet.getCell(`H${nilai.length + 15 + 3}`).value = 'Dosen Pengajar,';

      // worksheet.getCell(`H${nilai.length + 15 + 3}`).font = {
      //   name: 'Arial',
      //   size: 11,
      //   bold: true,
      // };

      worksheet.getCell(`G${nilai.length + 15 + 8}`).value =
        '___________________________';

      worksheet.getCell(`G${nilai.length + 15 + 8}`).font = {
        name: 'Arial',
        size: 11,
        bold: true,
      };

      // write to a new buffer

      const file = await workbook.xlsx.writeBuffer();

      return res.send(file);
    } catch (error) {
      console.log(`error`, error.status);
      if (error.status === 404) {
        throw new HttpException(error.message, HttpStatus.NOT_FOUND);
      } else {
        throw new HttpException(
          error.message,
          error.status || HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  @Post()
  create(@Body() createExcelFileDto: CreateExcelFileDto) {
    return this.excelFileService.create(createExcelFileDto);
  }

  @Get()
  findAll() {
    return this.excelFileService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.excelFileService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateExcelFileDto: UpdateExcelFileDto,
  ) {
    return this.excelFileService.update(+id, updateExcelFileDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.excelFileService.remove(+id);
  }
}
