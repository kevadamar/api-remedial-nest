import { EntityRepository, Repository } from "typeorm";
import { Nilai } from "./entities/nilai.entity";

@EntityRepository(Nilai)
export class NilaiRepository extends Repository<Nilai>{
    
}