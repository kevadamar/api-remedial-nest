export interface Nilai {
  id: number;
  presense: number;
  task: number;
  formative: number;
  behavior: number;
  mid_test: number;
  full_test: number;
  total: number;
  grade: GradeNilai;
}

export enum GradeNilai {
  A = 'A',
  B = 'B',
  C = 'C',
  D = 'D',
  E = 'E',
}
