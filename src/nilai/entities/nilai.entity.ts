import { Course } from 'src/courses/entities/course.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('nilai')
export class Nilai {
  @PrimaryGeneratedColumn('increment')
  id: number;

  // @Column()
  // presense: number;
  // @Column()
  // task: number;
  // @Column()
  // formative: number;
  // @Column()
  // behavior: number;
  // @Column()
  // mid_test: number;
  // @Column()
  // full_test: number;

  @Column()
  flag_activity: string;

  @Column('decimal', { precision: 5, scale: 2 })
  total: number;

  // @Column({ length: 2 })
  // grade: string;

  // fk
  @Column({ default: null })
  courseId: number;

  @Column({ default: null })
  mhsId: number;

  @Column({ type: 'timestamp', select: false })
  updated_at: Date;

  @Column({
    type: 'timestamp',
    select: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  created_at: Date;

  @Column({ select: false, default: false })
  is_deleted: boolean;

  // relations
  @ManyToOne(() => Course, (course) => course.nilai)
  @JoinColumn()
  course: Course;

  @ManyToOne(() => User, (user) => user.nilai)
  @JoinColumn()
  mhs: User;
}
