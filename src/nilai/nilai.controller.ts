import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Request,
  HttpStatus,
  Req,
  UseGuards,
} from '@nestjs/common';
import { NilaiService } from './nilai.service';
import { CreateNilaiDto } from './dto/create-nilai.dto';
import { UpdateNilaiDto } from './dto/update-nilai.dto';
import { Pagination } from 'src/pagination/pagination';
import { Nilai } from './entities/nilai.entity';
import { JwtAuthGuard } from 'src/auth/jwt.auth.guard';
import { PermissionsGuard } from 'src/permissions.guard';
import { Permissions } from 'src/permissions.decorator';

@Controller('nilai')
export class NilaiController {
  constructor(private readonly nilaiService: NilaiService) {}

  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @Permissions('Admin,Dosen')
  @Post()
  async create(@Body() createNilaiDto: CreateNilaiDto): Promise<Nilai> {
    return await this.nilaiService.create(createNilaiDto);
  }

  @Get('/dosen')
  async findAllByDosen(): Promise<Pagination<Nilai>> {
    return await this.nilaiService.getNilaiGroupByDosen();
  }

  @Get('/check')
  async findAllCheck(): Promise<any> {
    // return await this.nilaiService.findAll(2, 2, 3);
  }

  @Get()
  async findAll(@Request() request): Promise<Pagination<Nilai>> {
    return await this.nilaiService.getAllNilai({
      search: request.query.hasOwnProperty('search')
        ? request.query.search
        : '',
      dosen: request.query.hasOwnProperty('dosen') ? request.query.dosen : '',
      mahasiswa: request.query.hasOwnProperty('mahasiswa')
        ? request.query.mahasiswa
        : '',
      course: request.query.hasOwnProperty('course')
        ? request.query.course
        : '',
      limit: request.query.hasOwnProperty('size') ? request.query.size : 10,
      page: request.query.hasOwnProperty('page') ? request.query.page - 1 : 0,
    });
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<object> {
    const data = await this.nilaiService.findOne(+id);
    return {
      statusCode: HttpStatus.OK,
      message: 'Success Get Score',
      data,
    };
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateNilaiDto: UpdateNilaiDto) {
    return this.nilaiService.update(+id, updateNilaiDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.nilaiService.remove(+id);
  }
}
