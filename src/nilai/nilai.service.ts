import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Pagination } from 'src/pagination/pagination';
import { PaginationOptionsInterface } from 'src/pagination/pagination.options.interface';
import { getConnection, getRepository, Like } from 'typeorm';
import { CreateNilaiDto } from './dto/create-nilai.dto';
import { UpdateNilaiDto } from './dto/update-nilai.dto';
import { Nilai } from './entities/nilai.entity';
import { NilaiRepository } from './nilai.repository';

@Injectable()
export class NilaiService {
  constructor(
    @InjectRepository(NilaiRepository)
    private readonly nilaiRepository: NilaiRepository,
  ) {}

  async findAll(
    dosenId: number,
    courseId: number,
    semester: number,
    flag_activity: string,
  ): Promise<Nilai[]> {
    try {
      // console.log(dosenId, courseId, semester);
      const nilai = await this.nilaiRepository.find({
        relations: ['course', 'course.dosen', 'mhs', 'mhs.kelas'],
        where: {
          flag_activity,
          course: {
            dosenId,
            semester,
            id: courseId,
          },
        },
        order: { id: 'DESC' },
      });
      // const newnilai = [];
      // nilai.map((n) => {
      //   if (n.course?.dosenId === dosenId && n.course?.semester === semester) {
      //     newnilai.push(n);
      //   }
      // });
      console.log(`nilai`, nilai);
      return nilai;
    } catch (error) {
      console.log(`error`, error);
    }
  }

  async getAllNilai(
    options: PaginationOptionsInterface,
  ): Promise<Pagination<Nilai>> {
    const results = await this.nilaiRepository.find({
      where: {
        is_deleted: false,
        course: {
          name: Like(`%${options.course}%`),
          dosen: {
            full_name: Like(`%${options.dosen}%`),
          },
        },
        mhs: {
          full_name: Like(`%${options.mahasiswa}%`),
        },
      },
      take: options.limit,
      skip: options.page,
      order: { id: 'DESC' },
      relations: ['mhs', 'course', 'course.dosen'],
    });

    return new Pagination<Nilai>({
      status: HttpStatus.OK,
      data: results,
      total: results.length,
      message: 'Success Get Nilai',
    });
  }

  async getNilaiGroupByDosen(): Promise<Pagination<Nilai>> {
    const results = await getConnection()
      .createQueryBuilder()
      .select('nilai')
      .from(Nilai, 'nilai')
      .where('nilai.is_deleted = :is_deleted', { is_deleted: false })
      .leftJoinAndSelect('nilai.course', 'course')
      .leftJoinAndSelect('nilai.mhs', 'mhs')
      .leftJoinAndSelect('mhs.kelas', 'kelas')
      .leftJoinAndSelect('course.dosen', 'dosen')
      .groupBy('nilai.courseId')
      .getMany();
    return new Pagination<Nilai>({
      status: HttpStatus.OK,
      data: results,
      total: results.length,
      message: 'Success Get Nilai Group By Dosen',
    });
  }

  async create(nilaiDto: CreateNilaiDto): Promise<Nilai> {
    try {
      // console.log(`this.totalGrade(nilaiDto)`, this.totalGrade(nilaiDto));
      // return this.nilaiRepository.findOne(1);
      const result = this.nilaiRepository.save({
        ...nilaiDto,
        // ...this.totalGrade(nilaiDto),
      });

      // const result = this.nilaiRepository.find();

      return result;
    } catch (error) {
      throw new HttpException('Forbidden', HttpStatus.BAD_REQUEST);
    }
  }

  async findOne(id: number): Promise<Nilai> {
    try {
      const nilai = await this.nilaiRepository.findOne(id, {
        where: {
          is_deleted: false,
        },
      });
      if (!nilai) {
        throw new HttpException('Data not found.', HttpStatus.NOT_FOUND);
      }
      return nilai;
    } catch (error) {
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  async update(id: number, UpdateNilaiDto: UpdateNilaiDto): Promise<Nilai> {
    try {
      const nilai = await this.nilaiRepository.findOne(id);
      // get flag_activity from remedial
      // const remedial = await this.nilaiRepository.query(`SELECT
      // Nilai__course__remedial.flag_activity AS flag_activity
      // FROM
      //     nilai Nilai
      // LEFT JOIN course Nilai__course ON
      //     Nilai__course.id = Nilai.courseId
      // LEFT JOIN remedial Nilai__course__remedial ON
      //     Nilai__course__remedial.courseId = Nilai__course.id
      // WHERE
      //     (
      //         Nilai.courseId = ${nilai.courseId} AND Nilai.mhsId = ${nilai.mhsId} AND Nilai.is_deleted = FALSE AND Nilai__course__remedial.mhsId = ${nilai.mhsId}
      //     ) AND ( Nilai.id IN (1) )
      // `);

      // console.log('remedials', remedials, remedial);
      // return { remedials, remedial: remedial[0] };
      if (!nilai) {
        throw new HttpException('Data not found.', HttpStatus.NOT_FOUND);
      }
      const result = await this.nilaiRepository.save({
        ...UpdateNilaiDto,
        id,
      });
      const nilaiUpdated = { ...nilai, ...result };
      return nilaiUpdated;
    } catch (error) {
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  async remove(id: number) {
    try {
      const { affected } = await getConnection()
        .createQueryBuilder()
        .update(Nilai)
        .set({ is_deleted: true })
        .where('id = :id', { id })
        .andWhere('is_deleted = :is_deleted', { is_deleted: false })
        .execute();

      if (!affected) {
        throw new HttpException('Data not found.', HttpStatus.NOT_FOUND);
      }

      throw new HttpException('Data Deleted.', HttpStatus.OK);
    } catch (error) {
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  // private totalGrade(createNilaiDto: CreateNilaiDto | UpdateNilaiDto) {
  //   const { presense, task, behavior, formative, full_test, mid_test } =
  //     createNilaiDto;
  //   const total: number =
  //     presense * 0.1 +
  //     task * 0.2 +
  //     formative * 0.1 +
  //     behavior * 0.05 +
  //     mid_test * 0.25 +
  //     full_test * 0.3;

  //   let grade = 'E';
  //   if (total >= 80 && total <= 100) {
  //     grade = 'A';
  //   } else if (total >= 70 && total < 80) {
  //     grade = 'B';
  //   } else if (total >= 60 && total < 70) {
  //     grade = 'C';
  //   } else if (total >= 40 && total < 60) {
  //     grade = 'D';
  //   }
  //   console.log(total, grade);

  //   return { total, grade };
  // }
}
