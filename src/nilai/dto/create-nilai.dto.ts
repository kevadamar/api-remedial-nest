import { Transform } from 'class-transformer';
import { IsNotEmpty, IsNumber, IsOptional } from 'class-validator';

export class CreateNilaiDto {
  @IsOptional()
  id?: number;

  // @IsNotEmpty()
  // presense: number;

  // @IsNotEmpty()
  // task: number;

  // @IsNotEmpty()
  // formative: number;

  // @IsNotEmpty()
  // behavior: number;

  // @IsOptional()
  // mid_test: number;

  // @IsOptional()
  // full_test: number;

  @IsNotEmpty()
  total: number;

  @IsNotEmpty()
  flag_activity: string;

  @IsNumber()
  @Transform((value) => (isNaN(+value.value) ? 0 : +value?.value))
  courseId: number;

  @IsNumber()
  @Transform((value) => (isNaN(+value.value) ? 0 : +value?.value))
  @IsOptional()
  mhsId?: number;
}
