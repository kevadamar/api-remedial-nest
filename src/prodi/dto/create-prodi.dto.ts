import { IsNotEmpty, IsString } from 'class-validator';

export class CreateProdiDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsString()
  @IsNotEmpty()
  konsentrasi: string;
}
