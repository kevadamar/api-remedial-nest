import { Module } from '@nestjs/common';
import { ProdiService } from './prodi.service';
import { ProdiController } from './prodi.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProdiRepository } from './prodi.repository';

@Module({
  controllers: [ProdiController],
  providers: [ProdiService],
  imports: [TypeOrmModule.forFeature([ProdiRepository])],
})
export class ProdiModule {}
