import { UserDetail } from 'src/user-detail/entities/user-detail.entity';
import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('prodi')
export class Prodi {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  name: string;

  @Column()
  konsentrasi: string;

  @Column({ select: false, default: false })
  is_deleted: boolean;

  @Column({ type: 'timestamp' })
  updated_at: Date;

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
  })
  created_at: Date;

  //   relations
  @OneToMany(() => UserDetail, (userDetail) => userDetail.prodi)
  @JoinColumn()
  userDetail: UserDetail;
}
