import { EntityRepository, Repository } from 'typeorm';
import { Prodi } from './entities/prodi.entity';

@EntityRepository(Prodi)
export class ProdiRepository extends Repository<Prodi> {}
