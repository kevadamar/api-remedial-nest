import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Pagination } from 'src/pagination/pagination';
import { PaginationOptionsInterface } from 'src/pagination/pagination.options.interface';
import { Like } from 'typeorm';
import { CreateProdiDto } from './dto/create-prodi.dto';
import { UpdateProdiDto } from './dto/update-prodi.dto';
import { Prodi } from './entities/prodi.entity';
import { ProdiRepository } from './prodi.repository';

@Injectable()
export class ProdiService {
  constructor(
    @InjectRepository(ProdiRepository)
    private readonly prodiRepository: ProdiRepository,
  ) {}

  async create(createProdiDto: CreateProdiDto): Promise<Prodi> {
    try {
      const result = await this.prodiRepository.save(createProdiDto);
      return { ...result, ...createProdiDto };
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async findAll(
    options: PaginationOptionsInterface,
  ): Promise<Pagination<Prodi>> {
    try {
      const results = await this.prodiRepository.find({
        where: {
          name: Like(`%${options.name}%`),
          is_deleted: false,
        },
        relations: ['userDetail', 'userDetail.user'],
        take: options.limit,
        skip: options.page * options.limit,
        order: { id: 'DESC' },
      });

      const total = await this.prodiRepository.count();

      return new Pagination<Prodi>({
        last_page: Math.ceil(total / options.limit),
        status: HttpStatus.OK,
        data: results,
        total: results.length,
        message: 'Success Get Prodi',
      });
    } catch (error) {
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  async findOne(id: number): Promise<Prodi> {
    try {
      const prodi = await this.prodiRepository.findOne(id, {
        where: {
          is_deleted: false,
        },
      });
      if (!prodi) {
        throw new HttpException('Data not found.', HttpStatus.NOT_FOUND);
      }
      return prodi;
    } catch (error) {
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  // Update Prodi by id and return prodi
  async update(id: number, updateProdiDto: UpdateProdiDto): Promise<Prodi> {
    try {
      const prodi = await this.prodiRepository.findOne(id, {
        where: {
          is_deleted: false,
        },
      });
      if (!prodi) {
        throw new HttpException('Data not found.', HttpStatus.NOT_FOUND);
      }
      const result = await this.prodiRepository.update(id, updateProdiDto);
      return { ...prodi, ...updateProdiDto };
    } catch (error) {
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  // Delete Prodi by id and return prodi
  async delete(id: number): Promise<Prodi> {
    try {
      const prodi = await this.prodiRepository.findOne(id, {
        where: {
          is_deleted: false,
        },
      });
      if (!prodi) {
        throw new HttpException('Data not found.', HttpStatus.NOT_FOUND);
      }
      const result = await this.prodiRepository.update(id, {
        is_deleted: true,
      });
      return { ...prodi, is_deleted: true };
    } catch (error) {
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }
}
