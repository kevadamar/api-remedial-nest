import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpStatus,
  Req,
} from '@nestjs/common';
import { ProdiService } from './prodi.service';
import { CreateProdiDto } from './dto/create-prodi.dto';
import { UpdateProdiDto } from './dto/update-prodi.dto';
import { Prodi } from './entities/prodi.entity';
import { Pagination } from 'src/pagination/pagination';

@Controller('prodi')
export class ProdiController {
  constructor(private readonly prodiService: ProdiService) {}

  @Post()
  async create(@Body() createUserDto: CreateProdiDto): Promise<object> {
    const data = await this.prodiService.create(createUserDto);
    return {
      statusCode: HttpStatus.CREATED,
      message: 'Success Created',
      data,
    };
  }

  @Get()
  async findAll(@Req() request): Promise<Pagination<Prodi>> {
    return this.prodiService.findAll({
      name: request.query.hasOwnProperty('name') ? request.query.name : '',
      limit: request.query.hasOwnProperty('size')
        ? parseInt(request.query.size)
        : 10,
      page: request.query.hasOwnProperty('page')
        ? parseInt(request.query.page) - 1
        : 0,
    });
  }
  @Get(':id')
  async findOne(@Param('id') id: string): Promise<object> {
    const data = await this.prodiService.findOne(+id);
    return {
      statusCode: HttpStatus.OK,
      message: 'Success Get',
      data,
    };
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateProdiDto: UpdateProdiDto) {
    return this.prodiService.update(+id, updateProdiDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.prodiService.delete(+id);
  }
}
