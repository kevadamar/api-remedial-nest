import { Course } from 'src/courses/entities/course.entity';
import { Kelas } from 'src/kelas/entities/kelas.entity';
import { Nilai } from 'src/nilai/entities/nilai.entity';
import { Role } from 'src/role/entities/role.entity';
import { UserDetail } from 'src/user-detail/entities/user-detail.entity';
import {
  BeforeInsert,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import * as bcrypt from 'bcryptjs';
import { Exclude } from 'class-transformer';

@Entity('user')
export class User {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  full_name: string;

  @Column({ unique: true })
  email: string;

  @Column()
  username: string;

  @Column({ nullable: true })
  nim: string;

  @Column()
  @Exclude({ toPlainOnly: true })
  password: string;

  @Column({ type: 'timestamp', select: true })
  updated_at: Date;

  @Column({ default: false })
  is_deleted: boolean;

  // fk
  @Column({ default: null, select: false })
  roleId: number;

  @Column({ default: null, select: false })
  kelasId: number;

  @Column({
    type: 'timestamp',
    select: true,
    default: () => 'CURRENT_TIMESTAMP',
  })
  created_at: Date;

  // relations
  @OneToOne(() => UserDetail, (userDetail) => userDetail.user)
  @JoinColumn()
  userDetail: UserDetail;

  @OneToMany(() => Course, (course) => course.dosen)
  @JoinColumn()
  course: Course[];

  @OneToMany(() => Nilai, (nilai) => nilai.mhs)
  @JoinColumn()
  nilai: Nilai[];

  @ManyToOne(() => Role, (role) => role.user)
  @JoinColumn()
  role: Role;

  @ManyToOne(() => Kelas, (kelas) => kelas.user)
  kelas: Kelas;

  // encrpyt password
  @BeforeInsert()
  async hashPassword() {
    this.password = await bcrypt.hash(this.password, 8);
  }

  async validatePassword(
    password: string,
    oldPassword: string,
  ): Promise<boolean> {
    console.log('password', password, oldPassword);
    return await bcrypt.compare(password, this.password);
  }
}
