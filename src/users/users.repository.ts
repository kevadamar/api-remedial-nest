import { HttpException, HttpStatus } from '@nestjs/common';
import { UserDetail } from 'src/user-detail/entities/user-detail.entity';
import {
  EntityRepository,
  getConnection,
  getManager,
  Repository,
} from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

@EntityRepository(User)
export class UsersRepository extends Repository<User> {
  async updateUser(
    id: number,
    updateUserDto: UpdateUserDto,
  ): Promise<User | Error> {
    return;
  }

  async saveUser(createUserDto: CreateUserDto): Promise<User> {
    try {
      return await getManager().transaction(
        'READ COMMITTED',
        async (transactionalEntityManager) => {
          // const connection = getConnection();
          const userDetail = new UserDetail();
          for (const key in createUserDto.userDetail) {
            if (
              Object.prototype.hasOwnProperty.call(
                createUserDto.userDetail,
                key,
              )
            ) {
              const element = createUserDto.userDetail[key];
              userDetail[key] = element;
            }
          }
          // console.log(`userDetail`, userDetail)
          await transactionalEntityManager.save(userDetail);

          const user = new User();
          for (const key in createUserDto) {
            if (Object.prototype.hasOwnProperty.call(createUserDto, key)) {
              const element = createUserDto[key];
              user[key] = element;
            }
          }
          user.userDetail = userDetail;
          await transactionalEntityManager.save(user);
          return user;
        },
      );
    } catch (err) {
      // console.log(`err da`, err);
      if (err.code == 'ER_DUP_ENTRY' || err.errno == 1062) {
        throw new HttpException(err.message, HttpStatus.UNPROCESSABLE_ENTITY);
      } else {
        throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    }
  }
}
