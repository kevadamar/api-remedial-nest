import { PartialType } from '@nestjs/mapped-types';
import {
  IsNumber,
  IsOptional,
  IsString,
  IsNotEmpty,
  IsEmail,
  IsObject,
} from 'class-validator';
import { CreateUserDetailDto } from 'src/user-detail/dto/create-user-detail.dto';
import { UniqueOnDatabase } from 'src/validator/unique.validation';
import { User } from '../entities/user.entity';
import { CreateUserDto } from './create-user.dto';

export class UpdateUserDto {
  @IsNumber()
  @IsOptional()
  id?: number;

  @IsString()
  @IsNotEmpty()
  full_name: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  nim: string;

  @IsString()
  @IsEmail()
  @UniqueOnDatabase(User)
  @IsOptional()
  email: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  username: string;

  @IsString()
  @IsOptional()
  password: string;

  @IsNumber()
  @IsOptional()
  roleId: number;

  @IsNumber()
  @IsOptional()
  kelasId: number;

  // @IsNumber()
  // userDetailId: number;

  @IsObject()
  @IsNotEmpty()
  userDetail: CreateUserDetailDto;
}
