import {
  IsDefined,
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsObject,
  IsOptional,
  IsString,
} from 'class-validator';
import { CreateUserDetailDto } from 'src/user-detail/dto/create-user-detail.dto';
import { UniqueOnDatabase } from 'src/validator/unique.validation';
import { User } from '../entities/user.entity';

export class CreateUserDto {
  @IsNumber()
  @IsOptional()
  id?: number;

  @IsString()
  @IsNotEmpty()
  full_name: string;

  @IsString()
  @IsNotEmpty()
  nim: string;

  @IsString()
  @IsNotEmpty()
  @IsEmail()
  @UniqueOnDatabase(User)
  email: string;

  @IsString()
  @IsNotEmpty()
  username: string;

  @IsString()
  @IsNotEmpty()
  password: string;

  @IsNumber()
  @IsNotEmpty()
  roleId: number;

  @IsNumber()
  @IsOptional()
  kelasId: number;

  // @IsNumber()
  // userDetailId: number;

  @IsObject()
  @IsNotEmpty()
  userDetail: CreateUserDetailDto;
}
