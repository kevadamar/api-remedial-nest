import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Req,
  HttpStatus,
  UseGuards,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { Pagination } from 'src/pagination/pagination';
import { User } from './entities/user.entity';
import { UpdateUserDto } from './dto/update-user.dto';
import { JwtAuthGuard } from 'src/auth/jwt.auth.guard';
import { PermissionsGuard } from 'src/permissions.guard';
import { Permissions } from 'src/permissions.decorator';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @Permissions('Admin,Akademik')
  @Post()
  async create(@Body() createUserDto: CreateUserDto): Promise<object> {
    const data = await this.usersService.create(createUserDto);
    return {
      statusCode: HttpStatus.CREATED,
      message: 'Success Created',
      data,
    };
  }

  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @Permissions('Admin,Akademik')
  @Get()
  async findAll(@Req() request): Promise<Pagination<User>> {
    return this.usersService.findAll(request.query.hasOwnProperty('flag'), {
      name: request.query.hasOwnProperty('name') ? request.query.name : '',
      limit: request.query.hasOwnProperty('size')
        ? parseInt(request.query.size)
        : 10,
      page: request.query.hasOwnProperty('page')
        ? parseInt(request.query.page) - 1
        : 0,
    });
  }

  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @Permissions('Admin,Akademik')
  @Get(':id')
  async findOne(@Param('id') id: string): Promise<object> {
    const data = await this.usersService.findOne(+id);
    return {
      statusCode: HttpStatus.OK,
      message: 'Success Get',
      data,
    };
  }

  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @Permissions('Admin,Akademik')
  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateuserDto: UpdateUserDto,
  ): Promise<object> {
    const data = await this.usersService.update(+id, updateuserDto);
    return {
      statusCode: HttpStatus.OK,
      message: 'Success Update',
      data,
    };
  }

  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @Permissions('Admin,Akademik')
  @Delete(':id')
  async remove(@Param('id') id: string): Promise<object> {
    const data = await this.usersService.remove(+id);
    return {
      statusCode: HttpStatus.OK,
      message: 'Success Delete',
      data,
    };
  }
}
