import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Pagination } from 'src/pagination/pagination';
import { PaginationOptionsInterface } from 'src/pagination/pagination.options.interface';
import { getConnection, Like } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { UsersRepository } from './users.repository';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UsersRepository)
    private readonly usersRepository: UsersRepository,
  ) {}

  async login(user: string): Promise<User> {
    try {
      // const result = await this.usersRepository.findOne({
      //   where: [
      //     {
      //       username: Like(`%${user}%`),
      //     },
      //     {
      //       email: Like(`%${user}%`),
      //     },
      //   ],
      //   relations: ['role'],
      // });
      const result = await this.usersRepository
        .createQueryBuilder('user')
        .where('LOWER(username) = LOWER(:username)', { username: user })
        .orWhere('LOWER(email) = LOWER(:email)', { email: user })
        .leftJoinAndSelect('user.role', 'role')
        .leftJoinAndSelect('user.userDetail', 'userDetail')
        .getOne();
      return result;
    } catch (error) {
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  async create(createUserDto: CreateUserDto): Promise<User> {
    try {
      // console.log(`createUserDto`, createUserDto);
      const result = await this.usersRepository.saveUser(createUserDto);
      return result;
    } catch (error) {
      // console.log(`error`, error)
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  async findAll(
    flag = 'all',
    options: PaginationOptionsInterface,
  ): Promise<Pagination<User>> {
    try {
      let withFlag = {};
      if (flag) {
        withFlag = {
          is_deleted: false,
          role: {
            name: 'Dosen',
          },
        };
      }
      const results = await this.usersRepository.find({
        where: {
          full_name: Like(`%${options.name}%`),
          // is_deleted: false,
          ...withFlag,
        },
        relations: [
          'userDetail',
          'kelas',
          'nilai',
          'role',
          'course',
          'userDetail.prodi',
        ],
        take: options.limit,
        skip: options.page * options.limit,
        order: { id: 'DESC' },
      });

      const total = await this.usersRepository.count();

      return new Pagination<User>({
        last_page: Math.ceil(total / options.limit),
        status: HttpStatus.OK,
        data: results,
        total: results.length,
        message: 'Success Get Users',
      });
    } catch (error) {
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  async findOne(id: number): Promise<User> {
    try {
      const role = await this.usersRepository.findOne(id, {
        relations: [
          'userDetail',
          'userDetail.prodi',
          'kelas',
          'nilai',
          'role',
          'course',
        ],
      });
      if (!role) {
        throw new HttpException('User not found.', HttpStatus.NOT_FOUND);
      }
      return role;
    } catch (error) {
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  async update(
    id: number,
    updateUserDto: UpdateUserDto,
  ): Promise<User | object> {
    try {
      // console.log(`error`)
      const user = await this.usersRepository.findOne(id, {
        relations: ['userDetail'],
      });
      if (!user) {
        throw new HttpException('User not found.', HttpStatus.NOT_FOUND);
      }
      const result = await this.usersRepository.saveUser({
        ...updateUserDto,
        userDetail: { ...updateUserDto.userDetail, id: user.userDetail?.id },
        id,
      });

      const userUpdated = { ...user, ...result };
      return userUpdated;
    } catch (error) {
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  async remove(id: number) {
    try {
      const { affected } = await getConnection()
        .createQueryBuilder()
        .update(User)
        .set({ is_deleted: true })
        .where('id = :id', { id })
        .andWhere('is_deleted = :is_deleted', { is_deleted: false })
        .execute();

      if (!affected) {
        throw new HttpException('Data not found.', HttpStatus.NOT_FOUND);
      }

      throw new HttpException('Data Deleted.', HttpStatus.OK);
    } catch (error) {
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }
}
