import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpStatus,
  Req,
} from '@nestjs/common';
import { RoleService } from './role.service';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { Pagination } from 'src/pagination/pagination';
import { Role } from './entities/role.entity';

@Controller('role')
export class RoleController {
  constructor(private readonly roleService: RoleService) {}

  @Post()
  async create(@Body() createRoleDto: CreateRoleDto): Promise<object> {
    const data = await this.roleService.create(createRoleDto);
    return {
      statusCode: HttpStatus.CREATED,
      message: 'Success Created',
      data,
    };
  }

  @Get()
  async findAll(@Req() request): Promise<Pagination<Role>> {
    return this.roleService.findAll({
      name: request.query.hasOwnProperty('name') ? request.query.name : '',
      limit: request.query.hasOwnProperty('size')
        ? parseInt(request.query.size)
        : 10,
      page: request.query.hasOwnProperty('page')
        ? parseInt(request.query.page) - 1
        : 0,
    });
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<object> {
    const data = await this.roleService.findOne(+id);
    return {
      statusCode: HttpStatus.OK,
      message: 'Success Get',
      data,
    };
  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateRoleDto: UpdateRoleDto,
  ): Promise<object> {
    const data = await this.roleService.update(+id, updateRoleDto);
    return {
      statusCode: HttpStatus.OK,
      message: 'Success Update',
      data,
    };
  }

  @Delete(':id')
  async remove(@Param('id') id: string): Promise<object> {
    const data = await this.roleService.remove(+id);
    return {
      statusCode: HttpStatus.OK,
      message: 'Success Delete',
      data,
    };
  }
}
