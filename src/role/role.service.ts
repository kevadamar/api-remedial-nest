import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Pagination } from 'src/pagination/pagination';
import { PaginationOptionsInterface } from 'src/pagination/pagination.options.interface';
import { getConnection, Like } from 'typeorm';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { Role } from './entities/role.entity';
import { RoleRepository } from './role.repository';

@Injectable()
export class RoleService {
  constructor(
    @InjectRepository(RoleRepository)
    private readonly roleRepository: RoleRepository,
  ) {}

  async create(createRoleDto: CreateRoleDto): Promise<Role> {
    try {
      const result = await this.roleRepository.save(createRoleDto);
      return { ...result, ...createRoleDto };
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async findAll(
    options: PaginationOptionsInterface,
  ): Promise<Pagination<Role>> {
    try {
      const results = await this.roleRepository.find({
        where: {
          name: Like(`%${options.name}%`),
          is_deleted: false,
        },
        relations: ['user'],
        take: options.limit,
        skip: options.page * options.limit,
        order: { id: 'DESC' },
      });

      const total = await this.roleRepository.count();

      return new Pagination<Role>({
        last_page: Math.ceil(total / options.limit),
        status: HttpStatus.OK,
        data: results,
        total: results.length,
        message: 'Success Get Role',
      });
    } catch (error) {
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  async findOne(id: number): Promise<Role> {
    try {
      const role = await this.roleRepository.findOne(id, {
        where: {
          is_deleted: false,
        },
      });
      if (!role) {
        throw new HttpException('Data not found.', HttpStatus.NOT_FOUND);
      }
      return role;
    } catch (error) {
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  async update(id: number, updateRoleDto: UpdateRoleDto): Promise<Role> {
    try {
      const role = await this.roleRepository.findOne(id);
      if (!role) {
        throw new HttpException('Data not found.', HttpStatus.NOT_FOUND);
      }
      const result = await this.roleRepository.save({
        id,
        ...updateRoleDto,
      });
      const roleUpdated = { ...role, ...result };
      return roleUpdated;
    } catch (error) {
      // console.log(`error`, error);
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }

  async remove(id: number) {
    try {
      const { affected } = await getConnection()
        .createQueryBuilder()
        .update(Role)
        .set({ is_deleted: true })
        .where('id = :id', { id })
        .andWhere('is_deleted = :is_deleted', { is_deleted: false })
        .execute();

      if (!affected) {
        throw new HttpException('Data not found.', HttpStatus.NOT_FOUND);
      }

      throw new HttpException('Data Deleted.', HttpStatus.OK);
    } catch (error) {
      if (error.status) {
        throw new HttpException(error.message, error.status);
      } else {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }
}
