import { resolve } from 'path';
export interface Response<T> {
  statusCode: number;
  message: string;
  data: T;
}

export const pathImage = `${resolve(__dirname, '../../')}/file_upload/`;
